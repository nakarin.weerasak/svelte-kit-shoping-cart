import { browser } from '$app/env';
import { writable } from 'svelte/store';


export const darkTheme = writable<boolean>(false);


if (browser) {
    darkTheme.set(window.localStorage.getItem('darkTheme') == "true" ? true : false);
    darkTheme.subscribe(theme => {
        window.localStorage.setItem('darkTheme', `${theme}`);
    })
}