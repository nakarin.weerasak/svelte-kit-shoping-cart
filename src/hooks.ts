import cookie from 'cookie';
import type { Handle } from '@sveltejs/kit';
import { handleSession } from "svelte-kit-cookie-session";

const COOKIE_SECRET = (import.meta as any).env.VITE_COOKIE_SECRET;

export const handle: Handle = handleSession(
	{
		secret: COOKIE_SECRET,
		expires: 7 // 1 Day
	},
	async ({ request, resolve }) => {
		// Do anything you want here
		return resolve(request);
	}
);

/** @type {import('@sveltejs/kit').GetSession} */
export async function getSession({ locals }) {
	return locals.session.data;
}