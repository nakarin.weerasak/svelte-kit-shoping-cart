export type UserPermissionModel = {
    user?: UserModel,
    jwt?: string
}


export type UserModel = {
    confirmed?: boolean,
    blocked?: boolean,
    username?: string,
    email?: string,
    provider?: string,
    createdAt?: any,
    updatedAt?: any,
    role?: {
        _id?: string,
        name?: string,
        description?: string,
        type?: string,
        id?: string
    },
    id?: string
}