import Component from "./Component.svelte";


function createDialog(props: any) {
    if (typeof props === 'string') props = { title: props }

    const dialog = new Component({
        target: document.body,
        props,
        intro: true,
    });

    dialog.$on('destroy', () => {
        dialog.$destroy
    })

    return dialog;
}

const AlertConfirm = {
    alert: alert
}


async function alert(message: string) {
    const dialogConst = createDialog(message);
    const resp = await dialogConst.promise;
    dialogConst.$destroy();
    return resp;
}

export default AlertConfirm;
