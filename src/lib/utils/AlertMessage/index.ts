import Component from "./Component.svelte";

class AlertMessage {
    _dialogComponent;
    _html: string;
    constructor(htmlData: string) {
        this._html = htmlData;
    }
    _createDialog(): Component {
        const dialog = new Component({
            target: document.body,
            props: {
                html: this._html
            },
            intro: true,
        });
        return dialog;
    }

    async show() {
        this._dialogComponent = this._createDialog()
        const resp = this._dialogComponent.promise;
        return resp;
    }

    dispose(): void {
        this._dialogComponent.$destroy()
    }
}



export default AlertMessage;
