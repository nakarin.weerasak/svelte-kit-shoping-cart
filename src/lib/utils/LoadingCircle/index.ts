import Component from "./Component.svelte";

class LoadingCircle {
    dialogComponent;
    _createDialog(): Component {
        const dialog = new Component({
            target: document.body,
            props: {},
            intro: true,
        });
        return dialog;
    }

    show(): void {
        this.dialogComponent = this._createDialog()
    }

    dispose(): void {
        this.dialogComponent.$destroy()
    }
}



export default LoadingCircle;
