export async function post({ locals }) {
    locals.session.destroy = true;

    return {
        body: {
            ok: true,
        },
    };
}