
import type { RequestHandler, Response } from '@sveltejs/kit';
import type { Locals } from '$lib/types';
import { loginAPI } from './_auth';

export const post: RequestHandler<Locals, FormData> = async (request: any) => {
    const response: any = await loginAPI(request);
    if (response?.data?.jwt && response?.data?.user && request?.locals) {

        // console.log('request.locals.session *** AUTH ', request.locals)

        // set ค่า session หลัง Auth
        request.locals.session.data = {
            user: response.data.user,
            jwt: response.data.jwt
        }

        return {
            status: 200,
            body: {
                status: 200,
                user: response.data
            }
        };
    }
    return {
        status: 401,
        body: {
            status: 401,
            message: "401 Unauthorized"
        }
    };
};
