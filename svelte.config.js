import preprocess from 'svelte-preprocess';
import adapter from '@sveltejs/adapter-node';
import path from "path"

/** @type {import('@sveltejs/kit').Config} */
const config = {
    // Consult https://github.com/sveltejs/svelte-preprocess
    // for more information about preprocessors
    preprocess: [preprocess({
        "postcss": true
    })],

    kit: {
        adapter: adapter({
            // default options are shown
            out: 'build'
        }),
        // hydrate the <div id="svelte"> element in src/app.html
        target: '#svelte',
        vite: {
            resolve: {
                alias: {
                    'src': path.resolve('./src')
                }
            },
            server: {
                proxy: {
                    // '/service': 'http://134.209.212.127:3003',
                    // '/api': {
                    //     target: 'http://134.209.212.127:3003',
                    //     changeOrigin: true,
                    //     rewrite: (p) => p.replace(/^\/api/, '')
                    // }
                },
            },
        }
    }
};

export default config;